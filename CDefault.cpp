#include "CDefault.h"

void CDefault::execute()
{
	// 処理
	readSomeFile();

	// input("addNewStr!!");

	// output();

	writeSomeFile();
}

void CDefault::output()
{
	// 出力
	int i = 0;
	for (vector<string>::iterator itr = Vstr.begin(); itr != Vstr.end() ; ++itr)
	{
		cout << VVstr[0][i++] << endl;
	}
	i = 0;
	for (vector<string>::iterator itr = Vstr.begin(); itr != Vstr.end() ; ++itr)
	{
		cout << VVstr[1][i++] << endl;
	}
}

void CDefault::readSomeFile()
{
	vector<string> file;
	ifstream otherFile("otherFileName.conf");

    int num = 0;
	// 文字列の長さを読み込む
	while(!otherFile.eof())
	{
		string strHoge;
		otherFile >> strHoge;
		file.push_back(strHoge);
		num++;
	}
	otherFile.close();

	for (int i = 0; i < num; ++i)
	{
		readFile(file[i]);
		VVstr.push_back(Vstr);
	}
}

void CDefault::writeSomeFile()
{
	vector<string> file;
	ifstream otherFile("otherFileName.conf");

    int num = 0;
	// 文字列の長さを読み込む
	while(!otherFile.eof())
	{
		string strHoge;
		otherFile >> strHoge;
		file.push_back(strHoge);
		num++;
	}
	otherFile.close();

	for (int i = 0; i < num; ++i)
	{
		Vstr.clear();
		int j = 0;
		for (vector<string>::iterator itr = VVstr[i].begin(); itr != VVstr[i].end() ; ++itr)
		{
			Vstr.push_back(VVstr[i][j++]);
		}
		string strHoge = file[i];
		writeFile(strHoge);
		// cout << strHoge << endl;
	}
}

void CDefault::addFile(string filename)
{
	vector<string> file;
	ifstream otherFile("otherFileName.conf");

    int num = 0;
	// 文字列の長さを読み込む
	while(!otherFile.eof())
	{
		string strHoge;
		otherFile >> strHoge;
		file.push_back(strHoge);
		num++;
	}
	otherFile.close();
	file.push_back(filename);

	ofstream oFconf;
	oFconf.open("otherFileName.conf");

	for (int i = 0; i < num + 1; ++i)
	{
		if (i != 0)
		{
			// 最後の行に改行が来るとめんどくさいので
			oFconf << endl;
		}
		// 文字をtxtファイルに書き込む
		oFconf << file[i];
	}
}

