// C++

#ifndef __CDictionary_H__
#define __CDictionary_H__

#include <iostream>
#include <fstream>
#include <vector>
#include "abstractTemplate.h"

using namespace std;

// 辞書

class CDictionary : public abstractTemplate
{
public:
	CDictionary(){}
	~CDictionary(){}
	
	// 実行
	void execute();

	// 出力
	void output();

private:
	// Vstr[num]の要素をひとつ上と入れ替える
	void reOrderUp(int num);

	// Vstr[num]の要素をひとつ下と入れ替える
	void reOrderDown(int num);
};

#endif
