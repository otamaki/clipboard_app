#include "CDictionary.h"

void CDictionary::execute()
{
	// 処理
	readFile("CDictionary");
	
	// input("addNewStr!!");

	// reOrderUp(3);
	// reOrderDown(3);
	
	writeFile("CDictionary");
}

void CDictionary::output()
{
	// 出力
}

void CDictionary::reOrderUp(int num)
{
	if (num <= 0 || num >= realNum)
	{
		cout << "ERROR" << endl;
		return;
	}

	string strHoge;
	strHoge = Vstr[num];
	Vstr[num] = Vstr[num-1];
	Vstr[num-1] = strHoge;

	writeFile("CDictionary");
}

void CDictionary::reOrderDown(int num)
{
	if (num < 0 || num > realNum)
	{
		cout << "ERROR" << endl;
		return;
	}

	string strHoge;
	strHoge = Vstr[num];
	Vstr[num] = Vstr[num+1];
	Vstr[num+1] = strHoge;

	writeFile("CDictionary");
}

