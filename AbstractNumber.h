/*
 * AbstractNumber.h
 *
 *  Created on: 2016/05/24
 *      Author: nadu
 */

#ifndef ABSTRACTNUMBER_H_
#define ABSTRACTNUMBER_H_

class AbstractNumber{

private:

public:
	AbstractNumber(){};
	~AbstractNumber(){};

	bool isInteger(){return false;}
	bool isFloat(){return false;}
	bool isExponential(){return false;}
	bool isZero(){return false;}

	std::string toString();

};

#endif /* ABSTRACTNUMBER_H_ */
