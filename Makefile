# makefile
ClipboardApp:main.o CDefault.o CDictionary.o CHistory.o executeApp.o abstractTemplate.o
	g++ main.o CDefault.o CDictionary.o CHistory.o executeApp.o abstractTemplate.o -o ClipboardApp
main.o:main.cpp main.h AbstractNumber.h Double.h Integer.h RInteger.h
	g++ -c main.cpp
executeApp.o:executeApp.cpp CHistory.h CDictionary.h CDefault.h
	g++ -c executeApp.cpp
abstractTemplate.o:abstractTemplate.cpp abstractTemplate.h
	g++ -c abstractTemplate.cpp
CDefault.o:CDefault.cpp CDefault.h abstractTemplate.h
	g++ -c CDefault.cpp
CDictionary.o:CDictionary.cpp CDictionary.h abstractTemplate.h
	g++ -c CDictionary.cpp
CHistory.o:CHistory.cpp CHistory.h abstractTemplate.h CapacityQueue.h
	g++ -c CHistory.cpp
clean:
	rm -f main.o CDefault.o CDictionary.o CHistory.o executeApp.o abstractTemplate.o