// C++

#ifndef __CDefault_H__
#define __CDefault_H__

#include <iostream>
#include "abstractTemplate.h"

// 履歴管理

class CDefault : public abstractTemplate
{
public:
	CDefault(){}
	~CDefault(){}
	
	// 実行
	void execute();

	// 出力
	void output();

private:
	// 複数ファイル読み込み
	void readSomeFile();

	// 複数ファイル書き込み
	void writeSomeFile();

	// 複数ファイルのためのVector
	vector<vector<string> > VVstr;

	// ファイルの追加
	void addFile(string filename);
};

#endif