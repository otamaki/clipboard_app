#include "abstractTemplate.h"

void abstractTemplate::input(string str)
{
    // 入力
    addNewStr(str, "test");
    writeFile("test");
}

void abstractTemplate::readFile(string filename)
{
	string strTxt = filename + ".txt";
	string strConf = filename + ".conf";
	ifstream rFconf(strConf);

    vector<int> length;
    Vstr.clear();

    int num = 0;
	// 文字列の長さを読み込む
	while(!rFconf.eof())
	{
		int iHoge;
		rFconf >> iHoge;
		length.push_back(iHoge);
		num++;
	}
	rFconf.close();

    // 読み込んだ長さまで文字列を読み込む
    if (FILE *fp = fopen(strTxt.c_str(), "r"))
    {
    	int i = 0;
    	char buf[1024];
    	// 一行ずつ読み込み
    	while ((fgets(buf, sizeof(buf), fp)) != NULL)
    	{
    		string strHoge = buf;
    		if (Vstr.empty())
    		{
    			// 最初はvectorが空なのでセグるの回避のために代入
    			Vstr.push_back(strHoge);
    		}
    		else if (Vstr[i].size() <= length[i])
    		{
    			// 文字が足りない場合は次の行を追加
    			Vstr[i] = Vstr[i] + strHoge;
    		}
    		else
    		{
    			// 前の行の改行を削除して次の要素を追加
    			Vstr[i].pop_back();
    			Vstr.push_back(strHoge);
    			i++;
    		}

    		if (i >= num) break;
    	}
    	fclose(fp);
    }
    realNum += num;
}

void abstractTemplate::writeFile(string filename)
{
	ofstream wFtxt, wFconf;
	string strTxt = filename + ".txt";
	string strConf = filename + ".conf";
	wFtxt.open(strTxt);
	wFconf.open(strConf);

	for (int i = 0; i < realNum; ++i)
	{
		if (i != 0)
		{
			// 最後の行に改行が来るとめんどくさいので
			wFtxt << endl;
			wFconf << endl;
		}
		// 文字をtxtファイルに書き込む
		wFtxt << Vstr[i];
		// 文字数をconfファイルに書き込む。多バイト文字では文字数と一致しない
		wFconf << Vstr[i].size();
	}
}

void abstractTemplate::addNewStr(string new_str, string filename)
{
    Vstr.push_back(new_str);
    realNum++;
    writeFile(filename);
}

