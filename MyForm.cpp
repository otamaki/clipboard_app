#include "MyForm.h"

using namespace System;
using namespace System::Windows::Forms;

[STAThreadAttribute]
int main(array<String^>^ args) {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	// gcnew 1 で付けたプロジェクト名::2 で付けたForm名()
	// ここでは（gcnew Project1::MyForm()）
	Application::Run(gcnew clipbord_app::MyForm());
	return 0;
}