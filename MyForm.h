#pragma once
#using<system.dll>

namespace clipbord_app {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	using namespace System::IO;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;


	private: System::Windows::Forms::Label^  label1;

	private: System::Windows::Forms::ListBox^  listBox1;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  button8;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->listBox1 = (gcnew System::Windows::Forms::ListBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button1->Location = System::Drawing::Point(28, 295);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(131, 37);
			this->button1->TabIndex = 0;
			this->button1->Text = L"クリップボード履歴";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button2->Location = System::Drawing::Point(185, 295);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(131, 37);
			this->button2->TabIndex = 1;
			this->button2->Text = L"個人辞書";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button3->Location = System::Drawing::Point(344, 295);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(131, 37);
			this->button3->TabIndex = 2;
			this->button3->Text = L"LaTeX";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label1->Location = System::Drawing::Point(25, 19);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(241, 15);
			this->label1->TabIndex = 5;
			this->label1->Text = L"ボタンをクリックすると表示が更新されます";
			this->label1->Click += gcnew System::EventHandler(this, &MyForm::label1_Click);
			// 
			// listBox1
			// 
			this->listBox1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->listBox1->FormattingEnabled = true;
			this->listBox1->HorizontalScrollbar = true;
			this->listBox1->ItemHeight = 15;
			this->listBox1->Location = System::Drawing::Point(28, 54);
			this->listBox1->Name = L"listBox1";
			this->listBox1->Size = System::Drawing::Size(316, 214);
			this->listBox1->TabIndex = 7;
			this->listBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::listBox1_SelectedIndexChanged);
			// 
			// button4
			// 
			this->button4->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button4->Location = System::Drawing::Point(373, 98);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(88, 43);
			this->button4->TabIndex = 8;
			this->button4->Text = L"コピー";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// button5
			// 
			this->button5->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button5->Location = System::Drawing::Point(373, 191);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(88, 43);
			this->button5->TabIndex = 9;
			this->button5->Text = L"辞書登録";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
			// 
			// button6
			// 
			this->button6->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button6->Location = System::Drawing::Point(373, 98);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(88, 43);
			this->button6->TabIndex = 8;
			this->button6->Text = L"コピー";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &MyForm::button6_Click);
			// 
			// button7
			// 
			this->button7->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button7->Location = System::Drawing::Point(373, 191);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(88, 43);
			this->button7->TabIndex = 9;
			this->button7->Text = L"削除";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &MyForm::button7_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label2->Location = System::Drawing::Point(39, 63);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(129, 15);
			this->label2->TabIndex = 6;
			this->label2->Text = L"行列を決めてください";
			// 
			// textBox1
			// 
			this->textBox1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox1->Location = System::Drawing::Point(54, 110);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 26);
			this->textBox1->TabIndex = 7;
			// 
			// textBox2
			// 
			this->textBox2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox2->Location = System::Drawing::Point(240, 110);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(100, 26);
			this->textBox2->TabIndex = 8;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label3->Location = System::Drawing::Point(181, 113);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(29, 19);
			this->label3->TabIndex = 9;
			this->label3->Text = L"×";
			// 
			// button8
			// 
			this->button8->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->button8->Location = System::Drawing::Point(324, 194);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(84, 38);
			this->button8->TabIndex = 10;
			this->button8->Text = L"決定";
			this->button8->UseVisualStyleBackColor = true;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(507, 374);
//			this->Controls->Add(this->button8);
//			this->Controls->Add(this->label3);
//			this->Controls->Add(this->textBox2);
//			this->Controls->Add(this->textBox1);
//			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Name = L"MyForm";
			this->Text = L"クリップボード拡張しちゃいました";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {

	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		label1->Text = L"クリップボード履歴";
		if (this->Controls->Contains(listBox1)) {
			listBox1->Items->Clear();
			if (this->Controls->Contains(button6)) {
				this->Controls->Remove(button6);
				this->Controls->Remove(button7);
				this->Controls->Add(button4);
				this->Controls->Add(button5);
			}
		}
		else {
			if (this->Controls->Contains(button6)) {
				this->Controls->Remove(button6);
				this->Controls->Remove(button7);
			}
			if (this->Controls->Contains(label2)){
				this->Controls->Remove(label2);
				this->Controls->Remove(label3);
				this->Controls->Remove(textBox1);
				this->Controls->Remove(textBox2);
				this->Controls->Remove(button8);
			}
			if (!this->Controls->Contains(button4)){
				this->Controls->Add(listBox1);
				this->Controls->Add(button4);
				this->Controls->Add(button5);
			}
			else
			{
				listBox1->Items->Clear();
			}

		}

		StreamReader^ din = File::OpenText("clipbord.txt");
		String^ str;
		int count = 0;
		while ((str = din->ReadLine()) != nullptr)
		{
			count++;
			listBox1->Items->Add(str);
		}

	}
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		label1->Text = L"個人辞書";
		if (this->Controls->Contains(listBox1)) {
			listBox1->Items->Clear();
			if (this->Controls->Contains(button4)) {
				this->Controls->Remove(button4);
				this->Controls->Remove(button5);
				this->Controls->Add(button6);
				this->Controls->Add(button7);
			}
		}
		else {
			if (this->Controls->Contains(button4)) {
				this->Controls->Remove(button4);
				this->Controls->Remove(button5);
			}
			if (this->Controls->Contains(label2)) {
				this->Controls->Remove(label2);
				this->Controls->Remove(label3);
				this->Controls->Remove(textBox1);
				this->Controls->Remove(textBox2);
				this->Controls->Remove(button8);
			}
			if (!this->Controls->Contains(button6))
			{
				this->Controls->Add(listBox1);
				this->Controls->Add(button6);
				this->Controls->Add(button7);
			}
			else
			{
				listBox1->Items->Clear();
			}
		}

		StreamReader^ din = File::OpenText("dictionary.txt");
		String^ str;
		int count = 0;
		while ((str = din->ReadLine()) != nullptr)
		{
			count++;
			listBox1->Items->Add(str);
		}

	}
	private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
		label1->Text = L"LaTeX";
		if (this->Controls->Contains(listBox1)) {
			listBox1->Items->Clear();
			this->Controls->Remove(listBox1);
			if (this->Controls->Contains(button4)) {
				this->Controls->Remove(button4);
				this->Controls->Remove(button5);
			}
			if (this->Controls->Contains(button6)) {
				this->Controls->Remove(button6);
				this->Controls->Remove(button7);
			}
				this->Controls->Add(label2);
				this->Controls->Add(label3);
				this->Controls->Add(textBox1);
				this->Controls->Add(textBox2);
				this->Controls->Add(button8);
		}
		else
		{
			if (this->Controls->Contains(button4)) {
				this->Controls->Remove(button4);
				this->Controls->Remove(button5);
			}
			if (this->Controls->Contains(button6)) {
				this->Controls->Remove(button6);
				this->Controls->Remove(button7);
			}
			if (!this->Controls->Contains(label2)) {
				this->Controls->Add(label2);
				this->Controls->Add(label3);
				this->Controls->Add(textBox1);
				this->Controls->Add(textBox2);
				this->Controls->Add(button8);
			}
		}

		/*		listBox1->Items->Clear();
				StreamReader^ din = File::OpenText("LaTeX.txt");
				String^ str;
				int count = 0;
				while ((str= din->ReadLine()) != nullptr)
				{
					count++;
					listBox1->Items->Add(str);
				}

				*/
	}
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void listBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) {
	}

	};
}