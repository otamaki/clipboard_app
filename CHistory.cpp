#include "CHistory.h"

void CHistory::execute()
{
	// テスト用
	readFile("CHistory");
	
	input("addNewStr!!");

	writeFile("CHistory");
}

void CHistory::output()
{
	// 出力
}

void CHistory::input(string str)
{
	// 入力
	addNewStr(str, "CHistory");

	// 履歴は最大HISTORY_NUM個に保つ
	setHISTORY_NUM();
	writeFile("CHistory");
}

void CHistory::changeHISTORY_NUM(int num)
{
	// 履歴の件数を変えられるように
	HISTORY_NUM = num;
}

void CHistory::setHISTORY_NUM()
{
	while(realNum > HISTORY_NUM)
	{
		Vstr.erase(Vstr.begin());
		realNum--;
	}
}

