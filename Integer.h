/*
 * Integer.h
 *
 *  Created on: 2016/05/24
 *      Author: nadu
 */

#ifndef INTEGER_H_
#define INTEGER_H_

#include <string>
#include <sstream>
#include "AbstractNumber.h"

class Integer : public AbstractNumber{

private:
	int value;
	int length;
	std::string sign_padding(){
		return value >= 0 ? "\textcolor{white}{-}" : "";
	}
	std::string padding(){
		std::string pad = "";
		int i;
		for(i=getLen();i<length;i++)
			pad += "0";
		if(!pad.empty())
			pad = "\textcolor{white}{" + pad + "}";
		return pad;
	}
	std::string IntToString(int number)
	{
	  std::stringstream ss;
	  ss << number;
	  return ss.str();
	}

public:
	Integer(int value){this->value = value;this->length = 0;}
	Integer(int vlaue,int length){this->value = value;this->length = length;}
	Integer(std::string sequence){this->value = parse(sequence);this->length = 0;}
	~Integer(){};
	bool isInteger(){return true;}
	bool isZero(){return value == 0;}
	void setLength(int length){this->length = length;}
	void setValue(int value){this->value = value;}
	void setValue(std::string sequence){this->value = parse(sequence);}
	int getValue(){return value;}
	std::string toString(){
		std::string values = IntToString(this->value);
		return std::string("$") + this->padding() + this->sign_padding() + values + std::string("$");
	}
	static int parse(std::string sequence){
		int value = 0;
		bool isMinus = false;
		bool isContinue = true;
		int i = 0;

		/* 符号認識 */
		if(sequence[0] == '-'){
			isMinus = true;
			i++;
		}
		while((i < (int)sequence.size()) && isContinue){
			switch(sequence[i]){
			case ',':
			case '_':
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				value *= 10;
				value += sequence[i] - '0';
				break;
			default:
				isContinue = false;
				break;
			}
		}
		if(isMinus)
			value *= -1;
		return value;
	}
	int getLen(){
		int i=1;
		int v = (value >= 0 ? value : -value);
		while(v >= 10){
			v /= 10;
			i++;
		}
		return i;
	}
};


#endif /* INTEGER_H_ */
