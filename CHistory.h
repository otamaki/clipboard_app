// C++

#ifndef __CHistory_H__
#define __CHistory_H__

#include <iostream>
#include <fstream>
#include <vector>
#include "abstractTemplate.h"
#include "CapacityQueue.h"

using namespace std;

// 履歴管理

// #define HISTORY_NUM 10

class CHistory : public abstractTemplate
{
public:
	CHistory(){HISTORY_NUM = realNum;}
	~CHistory(){}
	
	// 実行
	void execute();

	// 入力(オーバーライド)
	void input(string str);
	
	// 出力
	void output();

private:
	// 履歴の件数
	int HISTORY_NUM;

	void changeHISTORY_NUM(int num);

	// 履歴がHISTORY_NUMより多くなったら削除する
	void setHISTORY_NUM();
};

#endif
