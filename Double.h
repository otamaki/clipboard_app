/*
 * Double.h
 *
 *  Created on: 2016/05/24
 *      Author: nadu
 */

#ifndef DOUBLE_H_
#define DOUBLE_H_

#include "RInteger.h"

class Double : public AbstractNumber{
private:
	double value = 0.0;
	Integer integer;
	RInteger rinteger;
	void setParameters(){
		double d = value;
		int i = (int)d;
		int v = 0;
		int temp;
		d -= i;
		while(d > 0){
			v *= 10;
			d *= 10;
			temp = (int)d;
			v += temp;
			d -= temp;
		}

		integer.setValue(i);
		rinteger.setValue(v);
	}

public:
	Double(double value){this->value = value;setParameters();}
	Double(std::string sequence){setValue(sequence);}
	bool isFloat(){return true;}
	bool isZero(){return integer.isZero() && rinteger.isZero();}
	void setValue(std::string sequence){
		int i;
		for(i=0;i<sequence.size();i++){
			if(!('0' <= sequence[i] && sequence[i] <= '9')){
				if(sequence[i] == '.')
					break;
				if(sequence[i] != '-' && sequence[i] != '_' && sequence[i] != ',')
					break;
			}
		}
		integer.setValue(sequence);
		rinteger.setValue(sequence + i + 1);
	}
	void setValue(double d){
		value = d;
		setParameters();
	}
	void setILength(int length){
		integer.setLength(length);
	}
	void setFLength(int length){
		rinteger.setLength(length);
	}
	int getILen(){
		return integer.getLen();
	}
	int getFLen(){
		return rinteger.getLen();
	}
	int getLen(){
		return getILen() + 1 + getFLen();
	}
	std::string toString(){
		return integer.toString() + "." + rinteger.toString();
	}

};



#endif /* DOUBLE_H_ */
