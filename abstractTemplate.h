// C++

#ifndef __abstractTemplate_H__
#define __abstractTemplate_H__

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

// 処理部の抽象クラス

class abstractTemplate
{
public:
	abstractTemplate(){realNum = 0;}
	~abstractTemplate(){}

	// 実行
	virtual void execute() = 0;
	// 入力
	virtual void input(string str);
	// 出力
	virtual void output() = 0;

protected:
	// ファイル読み込み(テスト)
	void readFile(string filename);

	// ファイル書き込み(テスト)
	void writeFile(string filename);

	// 辞書登録されている件数
	int realNum;

	// 文字配列
	vector<string> Vstr;

	// 引数で受け取った文字列をVectorに追加
	void addNewStr(string new_str, string filename);
};

#endif
