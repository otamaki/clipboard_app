/*
 * CapacityQueue.h
 *
 *  Created on: 2016/05/17
 *      Author: nadu
 */


#ifndef CAPACITYQUEUE_H_
#define CAPACITYQUEUE_H_

class CapacityQueue {

private:
	int LIMIT_SIZE;
	int size;
	std::vector<std::string> array;

public:
	/*
	 * 引数値の分だけ容量を持つキューの生成
	 */
	CapacityQueue(int capacity){
		LIMIT_SIZE = capacity;
		size = 0;
	}
	/*
	 * 容量32のキューの生成
	 */
	CapacityQueue(){
		LIMIT_SIZE = 32;
		size = 0;
	}
	/*
	 * デストラクタ(動くか分からん　パート2)
	 */
	~CapacityQueue(){}
	/*
	 * 要素の追加
	 * すでに入っている要素は先頭に移動
	 */
	void add(std::string str){
		std::string temp;
		int index = this->indexOf(str);
		if(index == -1){
			if(LIMIT_SIZE == size){//Queue is full.
				//temp = array[--size];//temp<-削除要素
				size--;
				array.erase(array.begin() + size);
			}
				array.insert(array.begin(),str);
				size++;
		}else{
			moveToTop(index);
		}
	}
	/*
	 * 要素の検索
	 */
	int indexOf(std::string str){
		int i=0;
		for(i=0;i<size;i++){
			if(str == array[i])
				return i;
		}
		return -1;
	}
	/*
	 * 引数添え字の要素を先頭に移動
	 */
	void moveToTop(int from){
		std::string str;
		int i;
		str = array[from];
		for(i=from;i>0;i--){
			array[i] = array[i-1];
		}
		array[0] = str;
	}

	std::string get(int index){
		if(index < size)
			return array[index];
		return "";
	}
	/*
	*容量ありQueueの配列(Vector)を返す
	*/
	std::vector<string> getVector(){
		return array;
	}

};

#endif /* CAPACITYQUEUE_H_ */