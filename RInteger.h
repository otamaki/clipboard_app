/*
 * RInteger.h
 *
 *  Created on: 2016/05/24
 *      Author: nadu
 */

#ifndef RINTEGER_H_
#define RINTEGER_H_

class RInteger{

private:
	int value = 0;
	int length = 0;//表示桁数
	void reduce(){
		while(value > 0){
			if(value % 10 == 0)
				value /= 10;
			else
				break;
		}
	}
	std::string padding(){
		std::string pad = "";
		int i;
		for(i=getLen();i<length;i++)
			pad += "0";
		return pad;
	}

public:
	RInteger(int value){this->value = value;reduce();}
	RInteger(int vlaue,int length){this->value = value;this->length = length;reduce();}
	RInteger(std::string sequence){setValue(sequence);}
	void setLength(int length){this->length = length;}
	void setValue(int value){this->value = value;reduce();}
	void setValue(std::string sequence){
		value = Integer::parse(sequence);
		reduce();
	}
	bool isZero(){
		return value == 0;
	}
	int getValue(){return value;}
	std::string toString(){
		return "$" + value + "\textcolor{white}{" + padding() + "}$";
	}
	int getLen(){
		reduce();
		int i=1;
		int v = value;
		while(v >= 10){
			v /= 10;
			i++;
		}
		return i;
	}

};


#endif /* RINTEGER_H_ */
